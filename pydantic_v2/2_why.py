"""
There are so many reasons to use pydantic.

1. Great IDE support during development
"""

from pydantic import BaseModel


data = {
    "name": {"first_name": "John", "last_name": "Doe"},
    "eye_color": "black",
    "weight": 4.2,
    "address": {
        "location": {
            "street": "123 Main St",
            "city": "Hannover",
            "state": "Lower Saxony",
            "zip": "4567"
        },
        "coordinates": {
            "latitude": 37.1234,
            "longitude": -122.4567
        }
    }
}

# print first name
print(data["name"]["first_name"])
# print zip
print(data["address"]["location"]["zip"])



class Name(BaseModel):
    first_name: str
    last_name: str

class Coordinates(BaseModel):
    latitude: float
    longitude: float

class Location(BaseModel):
    street: str
    city: str
    state: str
    zip: str

class Address(BaseModel):
    location: Location
    coordinates: Coordinates


class Person(BaseModel):
    name: Name
    eye_color: str
    weight: float
    address: Address


person = Person(**data)

# print first name
print(person.name.first_name)
# print zip
print(person.address.location.zip)
