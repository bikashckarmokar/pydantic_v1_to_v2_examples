from pydantic import BaseModel, field_validator, FieldValidationInfo


class Foo(BaseModel):
    z: str
    y: float
    x: int

    @field_validator("x", mode="before")
    @classmethod
    def check_x(cls, v: int, info: FieldValidationInfo) -> int:
        # info will get only those items which are declared above x in the model
        # orders matters here
        
        if v is None:
            v = int(info.data["y"]) # check the difference in v1 and v2
            return v
        return v


print(Foo(x=None, y=1.3, z="Test"))  # x=1

# @field_validator(mode="before") is used when you want to perform custom validation or data transformation before Pydantic's default validation.
# @field_validator(mode="after") is used when you want to perform additional validation or adjustments based on the validated data after Pydantic's default validation.