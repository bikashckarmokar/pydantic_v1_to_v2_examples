from pydantic import field_validator, model_validator, ConfigDict, BaseModel, Field


# Add default None to Optional string
class User(BaseModel):
    id: int
    name: str | None = None


# Replace Config class by model_config attribute
class User(BaseModel):
    user_id: int
    user_name: str
    model_config = ConfigDict(from_attributes=True)


class User(BaseModel):
    name: str
    model_config = ConfigDict(extra="forbid")


# Replace Field old parameters to new ones
class User(BaseModel):
    name: list[str] = Field(..., min_length=1)


# Replace decorators
class User(BaseModel):
    name: str

    @field_validator("name", mode="before")
    @classmethod
    def validate_name(cls, v):
        return v

    @model_validator(mode="before")
    @classmethod
    def validate_root(cls, values):
        return values
