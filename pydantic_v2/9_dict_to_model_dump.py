from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str = 'Jane Doe'

user = User(id='123')

assert isinstance(user.id, int)
# Note that '123' was coerced to an int and its value is 123

print(user.id)
print(type(user.id))

# this is changed in v2. The `dict` method is deprecated; use `model_dump`
# user_data = user.dict()
user_data = user.model_dump()
print(user_data)