from pydantic import StringConstraints, BaseModel
from typing_extensions import Annotated


class Person(BaseModel):
    name: str
    age: int
    email: Annotated[
        str, StringConstraints(pattern=r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")
    ]


# construct()
data = {"name": "John", "age": 25, "email": "john@example.com"}
person = Person.model_construct(**data)
print(person)

# copy()
person_copy = person.model_copy()
print(person_copy)


# json()
person_json = person.model_dump_json()
print(person_json)

# parse_obj()
new_data = {"name": "Alice", "age": 28, "email": "alice@example.com"}
new_person = Person.model_validate(new_data)
print(new_person)
