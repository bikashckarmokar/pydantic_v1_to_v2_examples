from pydantic import BaseModel


class TypeCheck(BaseModel):
    number: int
    value: int


data = TypeCheck(number="1", value=3.24)

print(data)

## The above code will not work in pydantic v2.
## automatic type conversion is removed where data loss may occur

# possible
## string to int

# not possible
## float to int

# more on https://docs.pydantic.dev/latest/usage/conversion_table/
