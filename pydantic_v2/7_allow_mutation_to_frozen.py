from pydantic import BaseModel, ConfigDict

class ImmutableModel(BaseModel):
    model_config = ConfigDict(frozen=True)

    value: int

    

# Create an instance of the ImmutableModel
model_instance = ImmutableModel(value=42)

# Attempt to modify the instance
# This will raise an exception since frozen is True
model_instance.value = 99  


# In v2 you need to use frozen if you want to make an Immutable Model