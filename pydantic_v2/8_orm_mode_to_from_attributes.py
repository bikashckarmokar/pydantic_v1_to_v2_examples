from pydantic import BaseModel, ConfigDict


class User(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    user_id: int
    user_name: str


# from sqlalchemy.orm import DeclarativeBase
# from sqlalchemy import Integer, String
# from sqlalchemy.orm import Mapped, mapped_column


# class Base(DeclarativeBase):
#     pass


# class UserModel(Base):
#     __tablename__ = "users"
#     user_id: Mapped[int] = mapped_column(Integer, primary_key=True)
#     user_name: Mapped[str] = mapped_column(String(512))

# why from_attributes=True
"""
The `orm_mode` setting in Pydantic's `Config` class is particularly beneficial when working with database models, especially in the context of an Object-Relational Mapping (ORM) framework like SQLAlchemy. It simplifies the process of serializing and deserializing data between your Python code and your database, offering several advantages:

1. **Automatic Conversion to Python Objects:** When fetching data from a database using an ORM, the result is often returned as ORM objects. With `orm_mode` enabled, Pydantic models can automatically convert these ORM objects into native Python objects, making it easier to work with the data in your application code.

2. **Serialization for API Responses:** When you want to expose your database records as API responses, `orm_mode` allows you to quickly convert ORM objects into JSON-compatible dictionaries without writing custom serialization code. This is particularly useful in web frameworks like FastAPI where you can directly use Pydantic models as response models.

3. **Deserialization for API Requests:** When receiving data from API requests, Pydantic models with `orm_mode` enabled can automatically parse and validate the incoming JSON data, and then convert it into ORM objects for storage in your database. This reduces the amount of boilerplate code required for data validation and manipulation.

4. **Data Validation:** Pydantic provides powerful data validation and automatic type conversion. Enabling `orm_mode` allows you to leverage this validation for both incoming data from API requests and outgoing data for API responses.

5. **Code Reusability:** By using the same Pydantic models for API inputs, database objects, and API outputs, you achieve consistency and reusability throughout your application. Changes to the data model are propagated seamlessly across different parts of your application.

6. **Consistency:** Using the same data models for serialization and deserialization ensures that the data is treated consistently across different parts of your application, leading to fewer errors and improved maintainability.

7. **Simplification:** Pydantic's `orm_mode` simplifies the codebase by reducing the need for manual data conversion and validation, resulting in cleaner and more readable code.

In summary, the benefit of using `orm_mode` with database models is that it streamlines the process of moving data between your application's code and your database, providing automatic conversion, validation, and serialization/deserialization of ORM objects, and reducing the need for writing repetitive and error-prone code.
"""