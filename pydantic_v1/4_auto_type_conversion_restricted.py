from pydantic import BaseModel


class TypeCheck(BaseModel):
    number: int
    value: int

data = TypeCheck(number='1', value=3.24)

print(data)

## This will print in pydantic v1
## number=1 value=3 valid_user=1

# v1 supported conversions
## string to int
## float to int 
