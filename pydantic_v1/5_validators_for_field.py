from pydantic import BaseModel, validator


class Foo(BaseModel):
    z: str
    y: float
    x: int    

    @validator("x", pre=True)
    def check_x(cls, v: int, values: dict[str, int | float | str]) -> int:
        # values will get only those items which are declared above x in the model
        # orders matters here
        if v is None:
            return int(values["y"]) # check the difference in v1 and v2
        return v


print(Foo(x=None, y=1.3, z="Test"))  # x=1

# @root_validator(pre=True) is used when you want to perform custom validation or data transformation before Pydantic's default validation.
# @root_validator(pre=False) is used when you want to perform additional validation or adjustments based on the validated data after Pydantic's default validation.