from datetime import datetime
import pprint
from pydantic import BaseModel, PositiveInt, ValidationError

pp = pprint.PrettyPrinter(indent=4)

class User(BaseModel):
    id: int  
    name: str = 'John Doe'  
    signup_ts: datetime | None  
    tastes: dict[str, PositiveInt]  


external_data = {
    'id': 123,
    'signup_ts': '2019-06-01 12:22',  
    'tastes': {
        'wine': 9,
        b'cheese': 7,  
        'cabbage': '1',  
    },
}

# user = User(**external_data)  

# print(user.dict())  

# If validation fails, Pydantic will raise an error with a breakdown of what was wrong:

external_data = {'id': 'not an int', 'tastes': {}}  

try:
    User(**external_data)  
except ValidationError as e:
    pp.pprint(e.errors())

# errors are less specific than Pydantic V2 version