from pydantic import BaseModel


class ImmutableModel(BaseModel):
    value: int

    class Config:
        allow_mutation = False


# Create an instance of the ImmutableModel
model_instance = ImmutableModel(value=42)

# Attempt to modify the instance
# This will raise an exception since allow_mutation is False
model_instance.value = 99


# allow_mutation is changed in v2 and you need to use frozen