# need to install separate package
# poetry add pydantic-settings

from pydantic import BaseModel, Field
from pydantic import BaseSettings


class RedisSettings(BaseModel):
    host: str = Field(default="localhost")
    port: int = Field(default=6379)
    password: str = Field(default=...)

    @property
    def dsn(self) -> str:
        return f"redis://:{self.password}@{self.host}:{self.port}/0"


class DatabaseSettings(BaseModel):
    host: str = Field(default="localhost")
    password: str = Field(default="pass")
    user: str = Field(default="user")
    name: str = Field(default="db")
    port: int = Field(default=3306)

    @property
    def dsn(self) -> str:
        return f"mysql+pymysql://{self.user}:{self.password}@{self.host}:{self.port}/{self.name}"


class Settings(BaseSettings):
    project_name = "Pydantic Version Upgrade"  # type declaration is optional

    redis: RedisSettings = {}  # RedisSettings()
    database: DatabaseSettings = {}  # DatabaseSettings()

    class Config:
        case_sensitive = False
        env_nested_delimiter = "__"


settings = Settings()

print(settings.dict())
