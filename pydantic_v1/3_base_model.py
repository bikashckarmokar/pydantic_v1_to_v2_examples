from pydantic import BaseModel, constr


class Person(BaseModel):
    name: str
    age: int
    email: constr(regex=r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")


# construct()
data = {"name": "John", "age": 25, "email": "john@example.com"}
person = Person.construct(**data)
print(person)

# copy()
person_copy = person.copy()
print(person_copy)

# json()
person_json = person.json()
print(person_json)

# parse_obj()
new_data = {"name": "Alice", "age": 28, "email": "alice@example.com"}
new_person = Person.parse_obj(new_data)
print(new_person)
