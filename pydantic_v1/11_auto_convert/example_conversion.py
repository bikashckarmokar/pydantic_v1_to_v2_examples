from pydantic import BaseModel, Extra, Field, validator, root_validator


# Add default None to Optional string
class User(BaseModel):
    id: int
    name: str | None


# Replace Config class by model_config attribute
class User(BaseModel):
    user_id: int
    user_name: str

    class Config:
        orm_mode = True


class User(BaseModel):
    name: str

    class Config:
        extra = Extra.forbid


# Replace Field old parameters to new ones
class User(BaseModel):
    name: list[str] = Field(..., min_items=1)


# Replace decorators
class User(BaseModel):
    name: str

    @validator("name", pre=True)
    def validate_name(cls, v):
        return v

    @root_validator(pre=True)
    def validate_root(cls, values):
        return values
